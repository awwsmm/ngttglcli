#!/bin/sh

# exit when any command fails
set -e

#------------------------------------------------------------------------------#
# assert that we're in the project root directory
#------------------------------------------------------------------------------#

ls_cargo_lock="$(ls -1 Cargo.lock 2> /dev/null || :)"
working_dir="$(pwd | awk -F'/' '{print $NF}')"

if [ "$ls_cargo_lock" != "Cargo.lock" ] || [ "$working_dir" != "ngttglcli" ]; then
  echo "this script must only be run from within the ngttglcli repo root directory"
  echo "please move to the project root directory and retry the release"
  exit 1
fi

#------------------------------------------------------------------------------#
# assert that we're on the master branch
#------------------------------------------------------------------------------#

# get the current git branch name
branch_name=$(git rev-parse --abbrev-ref HEAD)

if [ "$branch_name" != "master" ]; then
  echo "you are on the branch '$branch_name'"
  echo "release can only be performed on master branch"
  echo "please move to the master branch and retry the release"
  exit 1
fi;

#------------------------------------------------------------------------------#
# assert that we are on a clean, up-to-date master branch, with no untracked files
#------------------------------------------------------------------------------#

# get the status of the master branch (and remove all whitespace)
git_status=$(git status | tr -d "[:space:]")

expected_status=$(cat <<-END
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
END

)

# remove all whitespace so the comparison isn't spoiled by leading / trailing newlines
expected_status=$(echo "$expected_status" | tr -d "[:space:]")

if [ "$git_status" != "$expected_status" ]; then
  echo "'git status' reported \"Changes not staged for commit\", \"Untracked files\", or \"Your branch is behind 'origin/master'\""
  echo "release can only be performed on a clean master branch"
  echo "please resolve the above issues and retry the release"
  exit 1
fi;

# get up to date with remote master
git pull -q

#------------------------------------------------------------------------------#
# bump the version and create a new branch
#------------------------------------------------------------------------------#

case "$1" in
  major|minor|patch)
    echo "performing a $1 version bump..."
    cargo -q xtask bump "$1"
    ;;

  *)
    echo "unknown version bump: '$1'"
    echo "usage: ./release.sh {major,minor,patch}"
    echo "please use a known version bump and retry the release"
    exit 1
    ;;
esac

#------------------------------------------------------------------------------#
# create the release artifacts
#------------------------------------------------------------------------------#

# get new version
new_version="$(cargo run -- --version | awk -F' ' '{print $NF}')"

# move to new branch named after this version
git switch -c "$new_version"

# build the x86_64 binary
cross build -q --target x86_64-apple-darwin --release
x86_64_binary_arch="$(file -b target/x86_64-apple-darwin/release/ngttglcli)"

if [ "$x86_64_binary_arch" != "Mach-O 64-bit executable x86_64" ]; then
  echo "there was a problem creating the x86_64 binary"
  echo "aborting release process"
  exit 1
fi

# build the aarch64 binary
cross build -q --target aarch64-apple-darwin --release
aarch64_binary_arch="$(file -b target/aarch64-apple-darwin/release/ngttglcli)"

if [ "$aarch64_binary_arch" != "Mach-O 64-bit executable arm64" ]; then
  echo "there was a problem creating the aarch64 binary"
  echo "aborting release process"
  exit 1
fi

# build the Windows binary
brew install mingw-w64 --quiet
rustup -q target add x86_64-pc-windows-gnu
cargo build --target=x86_64-pc-windows-gnu --release
windows_binary_arch="$(file -b target/x86_64-pc-windows-gnu/release/ngttglcli.exe)"

if [ "$windows_binary_arch" != "PE32+ executable (console) x86-64, for MS Windows" ]; then
  echo "there was a problem creating the Windows binary"
  echo "aborting release process"
  exit 1
fi

# zip the binaries
cd target/x86_64-apple-darwin/release && tar -czf ngttglcli-x86_64.tar.gz ngttglcli && cd ../../../
cd target/aarch64-apple-darwin/release && tar -czf ngttglcli-aarch64.tar.gz ngttglcli && cd ../../../

# hash the tarballs
printf "\nbinary tarball SHAs\n"
shasum -a 256 target/*/release/ngttglcli*.tar.gz

# push to remote git repo
printf "\npushing to remote repo\n"
git add .
git commit -m "$new_version"
git push --set-upstream origin "$new_version"