use std::collections::HashMap;
use std::fs;
use std::hash::Hash;
use std::time::{Duration, SystemTime};

use serde::de::DeserializeOwned;
use serde::Serialize;

/// Attempts to read the JSON-formatted data from the specified file.
///
/// Will return [Err] if the file's data or metadata cannot be read, or if the cache is older than the `cache_max_age`.
pub fn read_from_cache<K, V>(
    cache_file_name: &str,
    cache_max_age: Duration,
) -> Result<HashMap<K, V>, String>
where
    K: Serialize + DeserializeOwned + Eq + Hash,
    V: Serialize + DeserializeOwned,
{
    // only if the cache file exists, can be read, and its modification time is <= max, do we use it
    let cache_age = fs::metadata(cache_file_name)
        .map_err(|error| error.to_string())
        .and_then(|metadata| metadata.modified().map_err(|error| error.to_string()))
        .and_then(|file_time| {
            SystemTime::now()
                .duration_since(file_time)
                .map_err(|error| error.to_string())
        });

    // only if the cache can be read and correctly converted to a Vec<T>, do we use it
    let cache_contents = fs::read_to_string(cache_file_name)
        .map_err(|error| error.to_string())
        .and_then(|contents| serde_json::from_str(&contents).map_err(|error| error.to_string()));

    match (cache_age, cache_contents) {
        (Ok(age), contents) if age.le(&cache_max_age) => contents,
        (Ok(age), _) => Err(format!(
            "expired cache (age: {:?}s, max age: {:?}s)",
            age.as_secs(),
            cache_max_age.as_secs()
        )),
        (Err(msg), _) => Err(msg),
    }
}

/// Writes the `data` to the specified file in JSON format.
pub fn overwrite_cache<K, V>(cache_file_name: &str, data: &HashMap<K, V>) -> Result<(), String>
where
    K: Serialize + DeserializeOwned + Eq + Hash,
    V: Serialize + DeserializeOwned,
{
    serde_json::to_string_pretty(data)
        .map_err(|_| String::from("Failed to serialize to JSON"))
        .and_then(|serialized| {
            fs::write(cache_file_name, serialized).map_err(|error| error.to_string())
        })
}

/// Updates some (but not all) of the cache.
///
/// Because some of the cache will not be updated, the file modification time is purposefully not updated.
///
/// This means that the age of the cache will not be reset, and it will expire at the expected time, as though it had not been updated.
pub fn update_cache<K, V>(
    cache_file_name: &str,
    cache_max_age: Duration,
    patch: &HashMap<K, V>,
) -> HashMap<K, V>
where
    K: Serialize + DeserializeOwned + Eq + Hash + Clone,
    V: Serialize + DeserializeOwned + Clone,
{
    // save the current modification time of the file
    let mtime = fs::metadata(cache_file_name).and_then(|metadata| metadata.modified());

    // get the current cache
    let mut cache: HashMap<K, V> =
        read_from_cache(cache_file_name, cache_max_age).unwrap_or_else(|_| HashMap::new());

    // update the cache with the patch
    for (key, value) in patch {
        cache.insert(key.clone(), value.clone());
    }

    // rewrite the file with the patched cache
    overwrite_cache(cache_file_name, &cache).expect("Failed to update cache");

    // set the modification time of the file to the original mtime
    // sometimes fails if we only just created the file above (race condition?)
    let _ = mtime
        .and_then(|time| filetime::set_file_mtime(cache_file_name, filetime::FileTime::from(time)));

    cache
}
