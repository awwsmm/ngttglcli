# `ngttglcli`

The NGTT GitLab CLI `ngttglcli` is useful for

- discovering projects with lots of Scala Steward branches and open Scala Steward MRs
- deleting those branches and closing those MRs ("declaring Scala Steward bankruptcy")

## Installing

`ngttglcli` can be installed using Homebrew.

You'll need a [GitLab SSH key](https://docs.gitlab.com/ee/user/ssh.html) configured to do this.

Add the `ngtt` tap with

```shell
brew tap cpc-tracktrace/ngtt git@gitlab.com:cpc-tracktrace/homebrew-ngtt.git
```

and install with

```shell
brew install ngttglcli
```

## Running

Print the version of the project with `-v` or `--version`

```shell
ngttglcli --version
```

Open the REPL by running `ngttglcli` with no arguments

```
$ ngttglcli
Connecting to cpc-tracktrace... connected!

>> 
```

Note that the tool requires a `GITLAB_PAT` environment variable to be set. If you don't already have one, you can create a GitLab Personal Access Token at [gitlab.com/-/profile/personal_access_tokens](https://gitlab.com/-/profile/personal_access_tokens).

In the rest of this README, prompts beginning with `>> ` are inside the `ngttglcli` REPL.

## Commands

### `help`

Prints the help menu.

```
>> help
Available commands:
  help                  -- prints this help menu
  fetch all             -- query GitLab for the list of projects and rewrite the cache
  fetch <search term>   -- query GitLab for projects matching the search term and update them in the cache
  projects              -- prints a list of known projects with their IDs
  scan all              -- query GitLab for Scala Steward MRs and branches for all projects and rewrite the cache
  scan <search term>    -- query GitLab for Scala Steward MRs and branches for projects matching the search term and update them in the cache
  report all            -- prints a scan report every project with a nonzero number of Scala Steward MRs or branches
  report <project id>   -- prints a scan report for the project with the specified ID
  clean <project id>    -- closes Scala Steward MRs and deletes Scala Steward branches for the project with the specified ID
  quit                  -- close the REPL (also: 'exit')

>> 
```

### `fetch all`

Fetches all projects from GitLab under the `GROUP_NAME` (`cpc-tracktrace`).

Populates the cache file at `.ngttglcli_projects`.

```
>> fetch all
Performing a forced cache update of the list of projects...

>> 
```

### `fetch <search term>`

Fetches all projects from GitLab under the `GROUP_NAME` (`cpc-tracktrace`) which match the `search term` (in their name, or description, or elsewhere)

```
>> fetch ssd
Search returned these projects...
  42527091: ssd-validation-fe @ cpc-tracktrace/qa/ssd-validation-fe
  39489405: ssd-validation-db @ cpc-tracktrace/data/ssd-validation-db
  38181748: international-service-standards @ cpc-tracktrace/domain/international-service-standards
  41080150: ssd-validation @ cpc-tracktrace/qa/ssd-validation
...updated them in the cache.

>> 
```

```
>> fetch service-standard-date
Search returned these projects...
  44182741: estimation-common @ cpc-tracktrace/domain/estimation-common
  32616975: service-standard-date @ cpc-tracktrace/domain/service-standard-date
...updated them in the cache.

>> 
```

### `projects`

Lists all known projects.

Only prints out the projects saved to the cache, and does not query GitLab, provided that the cache has not expired. (See `PROJECTS_CACHE_MAX_AGE_SECONDS`.)

The list of known projects can be refreshed by querying GitLab with `fetch all`.

```
>> projects
  29604102: ngtt-core @ cpc-tracktrace/domain/ngtt-core
  34517870: process-errors-ingress-api @ cpc-tracktrace/ingress/ingress-api/process-errors-ingress-api
  27054678: package-tracker @ cpc-tracktrace/domain/package-tracker
  ...
  29467239: external-reference-data @ cpc-tracktrace/domain/external-reference-data
  32934876: induction-ingress-api @ cpc-tracktrace/ingress/ingress-api/induction-ingress-api
  33011214: Ephemeral Environments @ cpc-tracktrace/infrastructure/ephemeral-environments

>> 
```

### `scan all`

Queries GitLab for Scala Steward branches and open Scala Steward MRs for all projects.

Refreshes the list of known projects (`fetch all`) before querying for branches and MRs.

This command can take some time (2-3 minutes), but the result is also saved in a cache, so subsequent calls will be faster.

```
>> scan all
Performing a forced cache update of the list of scan results...
Performing a forced cache update of the list of projects...
Progress: |=====================================...
  25562067: duplicatepackage has 33 open Scala Steward MR(s) and 175 Scala Steward branch(es)
  34551327: delivery has 23 open Scala Steward MR(s) and 44 Scala Steward branch(es)
  25004125: induction has 17 open Scala Steward MR(s) and 226 Scala Steward branch(es)
  ...
  41080150: ssd-validation has 0 open Scala Steward MR(s) and 0 Scala Steward branch(es)
  47556319: homebrew-ngtt has 0 open Scala Steward MR(s) and 0 Scala Steward branch(es)
  30183792: SBT Yourkit has 0 open Scala Steward MR(s) and 0 Scala Steward branch(es)

>> 
```

### `scan <search term>`

Fetches all projects from GitLab under the `GROUP_NAME` (`cpc-tracktrace`) which match the `search term` (in their name, or description, or elsewhere), and scans only those projects for Scala Steward branches and open Scala Steward MRs.

Does not refresh the cache of known projects, but does update the cache of scan results for the projects fetched.

```
>> scan ssd
Progress: |====|
Search returned these projects...
  38181748: international-service-standards has 9 open Scala Steward MR(s) and 9 Scala Steward branch(es)
  42527091: ssd-validation-fe has 0 open Scala Steward MR(s) and 0 Scala Steward branch(es)
  41080150: ssd-validation has 0 open Scala Steward MR(s) and 0 Scala Steward branch(es)
  39489405: ssd-validation-db has 0 open Scala Steward MR(s) and 0 Scala Steward branch(es)
...updated them in the cache.

>> 
```

```
>> scan service-standard-date
Progress: |==|
Search returned these projects...
  32616975: service-standard-date has 2 open Scala Steward MR(s) and 2 Scala Steward branch(es)
  44182741: estimation-common has 0 open Scala Steward MR(s) and 0 Scala Steward branch(es)
...updated them in the cache.

>> 
```

### `report all`

Prints a report to the console which lists every Scala Steward branch and open Scala Steward MR for every project which has been scanned.

```
>> report all

  25562067: duplicatepackage has 33 open Scala Steward MR(s) and 175 Scala Steward branch(es)
  MRs:
    MR #869: fix: Update authorized-returns-ingress-api from 1.4.13 to 1.5.10 [NGTT-13308]
    MR #864: fix: Update enrichment-client from 6.19.4 to 6.19.37 [NGTT-13308]
    MR #863: fix: Update sbt-innovapost from 6.5.1 to 6.6.4 [NGTT-13308]
  ...
  Branches:
    update/authorized-returns-ingress-api-1.2.14 -- "fix: Update authorized-returns-ingress-api from 1.2.12 to 1.2.14 [NGTT-13308]"
    update/authorized-returns-ingress-api-1.2.18 -- "fix: Update authorized-returns-ingress-api from 1.2.17 to 1.2.18 [NGTT-13308]"
    update/authorized-returns-ingress-api-1.2.25 -- "fix: Update authorized-returns-ingress-api from 1.2.24 to 1.2.25 [NGTT-13308]"
  ...
  34551327: delivery has 23 open Scala Steward MR(s) and 44 Scala Steward branch(es)
  MRs:
    MR #297: fix: Update unique-identifier-client from 3.8.4 to 3.9.4 [NGTT-13308]
    MR #296: fix: Update unique-identifier-client from 3.8.4 to 3.9.3 [NGTT-13308]
    MR #295: fix: Update delivery-ingress-api from 1.6.11 to 1.6.29 [NGTT-13308]
  ...
  33523631: measurements-ingress-api has 1 Scala Steward branch(es)
  Branches:
    update/est-ingress-api-1.3.3 -- "Setting version to 1.0.17-SNAPSHOT"

Total Scala Steward MRs open: 215
Total Scala Steward branches: 1809

>> 
```

### `report <project id>`

Prints a report only for the project with the specified id

```
>> report 28151248

  28151248: package-intransit has 1 open Scala Steward MR(s) and 1 Scala Steward branch(es)
  MRs:
    MR #1369: fix: Update nesting-domain-client from 3.58.167 to 3.58.170 [NGTT-13308]
  Branches:
    update/nesting-domain-client-3.58.170 -- "fix: Update nesting-domain-client from 3.58.167 to 3.58.170 [NGTT-13308]"

>> 
```

### `clean <project id>`

With confirmation from the user, deletes all Scala Steward branches and closes all open Scala Steward MRs for the specified project

**WARNING:** THIS IS A DESTRUCTIVE OPERATION.

Make sure you inspect every branch to be deleted, as they can be impossible to recover.

If you want to cancel the cleaning, you can hit CTRL-D, or simply type something which does not match the required confirmation phrase

```
>> clean 28151248

  28151248: package-intransit has 1 open Scala Steward MR(s) and 1 Scala Steward branch(es)
  MRs:
    MR #1369: fix: Update nesting-domain-client from 3.58.167 to 3.58.170 [NGTT-13308]
  Branches:
    update/nesting-domain-client-3.58.170 -- "fix: Update nesting-domain-client from 3.58.167 to 3.58.170 [NGTT-13308]"

  !!! WARNING !!!
    Cleaning this project will close all of the MRs and delete all of the branches listed above.
    Are you sure you wish to continue? If so, you must enter the following line exactly as it appears below:

    >> I! DECLARE! BANKRUPTCY!
    >> ^D
  Cleaning cancelled.

>> 
```

```
>> clean 28151248

  28151248: package-intransit has 1 open Scala Steward MR(s) and 1 Scala Steward branch(es)
  MRs:
    MR #1369: fix: Update nesting-domain-client from 3.58.167 to 3.58.170 [NGTT-13308]
  Branches:
    update/nesting-domain-client-3.58.170 -- "fix: Update nesting-domain-client from 3.58.167 to 3.58.170 [NGTT-13308]"

  !!! WARNING !!!
    Cleaning this project will close all of the MRs and delete all of the branches listed above.
    Are you sure you wish to continue? If so, you must enter the following line exactly as it appears below:

    >> I! DECLARE! BANKRUPTCY!
    >> I! DECLARE! BANKRUPTCY!

  CONFIRMED. Cleaning now...

    Deleting MR #1369: fix: Update nesting-domain-client from 3.58.167 to 3.58.170 [NGTT-13308]
    Deleting branch update/nesting-domain-client-3.58.170 -- "fix: Update nesting-domain-client from 3.58.167 to 3.58.170 [NGTT-13308]"

>> clean 28151248

  28151248: package-intransit has 0 open Scala Steward MR(s) and 0 Scala Steward branch(es)

  Nothing to clean

>> 
```

### `versions [repo] [tag-from] [tag-to] -o`

Attempts to display the list of commits that exist on a repo between two version tags. The `-o` flag will omit any commits made by the Scala Steward bot.

```
>> versions domain/expected-delivery-date 0.10.0 0.11.1

[v0.11.0]
✅ feat:NGTT-20984 "Update edd on itemoutfordelivery"

[v0.10.2]
🐞 fix:Adding consumer for PSIA messages

[v0.10.1]
🐞 fix:NGTT-21214 "Unit test for processing SsdUpdated event

[v0.10.0]
✅ feat:NGTT-21212 Command handler and event handler for SSDUpdated event

>>
```

### `quit` (also `exit`)

Closes the REPL.

```
>> quit

$ 
```

## Issues

To see open issues, and report any issues or make any feature requests, please go to https://gitlab.com/improving-andrew/ngttglcli/-/issues

## Developing and Contributing

To tinker with `ngttglcli` locally, follow the [Development Guide](README-DEV.md).

Then, if you want to contribute your changes to this repo, check out the [Contributors Guide](README-CONTRIBUTE.md).