use std::fs::{remove_dir_all, DirBuilder};
use std::ops::Deref;
use std::process::Command;
use std::{fs, io};

use regex::Regex;
use serde::Deserialize;

#[derive(Deserialize)]
struct CoverageSummary {
    message: String,
}

pub fn coverage(crate_name: &str) -> io::Result<()> {
    const COVERAGE_LIMIT: f32 = 6.6;

    let coverage_dir = format!("{crate_name}/coverage");
    let coverage_dir = coverage_dir.as_str();

    let html_dir = format!("{coverage_dir}/html");
    let html_dir = html_dir.as_str();

    // println!("clearing coverage directory");
    let _ = remove_dir_all(coverage_dir);

    // println!("creating coverage directory");
    DirBuilder::new().recursive(true).create(coverage_dir)?;

    println!("running tests...");
    Command::new("cargo")
        .env("CARGO_INCREMENTAL", "0")
        .env("RUSTFLAGS", "-C instrument-coverage")
        .env("LLVM_PROFILE_FILE", "coverage/cargo-test-%p-%m.profraw")
        .arg("test")
        .args(["-p", crate_name])
        .output()?;

    println!("generating HTML coverage report (for humans)...");
    Command::new("grcov")
        .arg(coverage_dir) // where are the *.profraw files?
        .args(["--binary-path", "target/debug"]) // where are the compiled Rust files?
        .args(["-s", crate_name]) // where is the Rust source code?
        .arg("--branch") // include branch coverage?
        .args(["--excl-start", "// coverage: off"])
        .args(["--excl-stop", "// coverage: on"])
        .args(["-t", "html"]) // what output format(s) do we want?
        .args(["-o", html_dir]) // where do we want the output?
        .output()?;

    let pct = read_coverage_pct_from_file(format!("{html_dir}/coverage.json")).unwrap();

    println!("Coverage: {}", pct);

    println!("generating cobertura coverage report (for CI)...");
    Command::new("grcov")
        .arg(coverage_dir) // where are the *.profraw files?
        .args(["--binary-path", "target/debug"]) // where are the compiled Rust files?
        .args(["-s", crate_name]) // where is the Rust source code?
        .arg("--branch") // include branch coverage?
        .args(["-t", "cobertura"]) // what output format(s) do we want?
        .args(["-o", "target"]) // where do we want the output?
        .output()?;

    if pct >= COVERAGE_LIMIT {
        Ok(())
    } else {
        Command::new("open")
            .arg(format!("{html_dir}/index.html"))
            .spawn()
            .unwrap();

        Err(io::Error::new(
            io::ErrorKind::Other,
            format!(
                "Failed to meet minimum coverage requirement: {} < {}",
                pct, COVERAGE_LIMIT
            ),
        ))
    }
}

fn read_coverage_pct_from_file(path: String) -> Result<f32, io::Error> {
    let file_contents = fs::read_to_string(path)?;
    parse_coverage_json(file_contents)
}

fn parse_coverage_json(json: String) -> Result<f32, io::Error> {
    let coverage_summary = serde_json::from_str::<CoverageSummary>(&json)?;

    let re = Regex::new(r"^([0-9]{1,3}\.[0-9]{1,3})%$").unwrap();

    re.captures(coverage_summary.message.deref())
        .and_then(|captures| {
            captures
                .get(1)
                .and_then(|capture| capture.as_str().parse::<f32>().ok())
        })
        .ok_or(io::Error::new(
            io::ErrorKind::InvalidData,
            "failed to parse coverage percentage",
        ))
}

#[cfg(test)]
mod tests {
    use crate::coverage::parse_coverage_json;

    #[test]
    fn parse_valid_json() {
        let example = String::from(
            r#"{"schemaVersion":1,"label":"coverage","message":"12.34%","color":"red"}"#,
        );
        let parsed = parse_coverage_json(example);

        match parsed {
            Ok(pct) => assert_eq!(pct, 12.34),
            Err(_) => panic!("expected valid JSON to be parsed correctly"),
        }
    }

    #[test]
    fn fail_to_parse_when_missing_message() {
        let example = String::from(r#"{"schemaVersion":1,"label":"coverage","color":"red"}"#);
        let parsed = parse_coverage_json(example);

        match parsed {
            Ok(_) => panic!("expected invalid JSON to fail to parse"),
            Err(error) => assert!(error.to_string().contains("missing field `message`")),
        }
    }

    #[test]
    fn fail_to_parse_when_message_invalid() {
        let example = String::from(
            r#"{"schemaVersion":1,"label":"coverage","message":"hello","color":"red"}"#,
        );
        let parsed = parse_coverage_json(example);

        match parsed {
            Ok(_) => panic!("expected invalid JSON to fail to parse"),
            Err(error) => assert_eq!(error.to_string(), "failed to parse coverage percentage"),
        }
    }
}
